import React, { Component } from "react";
import { connect } from "react-redux";
import Itemshoe from "./Itemshoe";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item, index) => {
      return <Itemshoe dataShoe={item} key={index} />;
    });
  };
  render() {
    console.log(this.props.shoeArr);
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    shoeArr: state.shoeReducer.shoeArr,
  };
};
export default connect(mapStateToProps)(ListShoe);
