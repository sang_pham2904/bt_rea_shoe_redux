import React, { Component } from "react";
import CartShoe from "./CartShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_Shoe_Shop extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-11">
            <ListShoe />
          </div>
          <div className="col-1">
            <CartShoe />
          </div>
        </div>
        <DetailShoe />
      </div>
    );
  }
}
