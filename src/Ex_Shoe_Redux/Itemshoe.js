import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_SHOE, CHANGE_DETAIL } from "./redux/constant/shoeConstant";

class Itemshoe extends Component {
  render() {
    let { dataShoe } = this.props;
    return (
      <div className="col-3 my-2">
        <div>
          <div className="card text-center">
            <div className="d-flex justify-content-center">
              <img
                className="card-img-top"
                style={{ width: "10vw" }}
                src={dataShoe.image}
              />
            </div>
            <div className="card-body">
              <h4 style={{ height: "70px" }} className="card-title">
                {dataShoe.name}
              </h4>
              <h6 className="card-text pb-4">
                $ {dataShoe.price.toLocaleString()}
              </h6>
              <p>{dataShoe.shortDescription}</p>
              <button
                onClick={() => this.props.handleAddToCart(dataShoe)}
                className="btn btn-primary mx-2"
              >
                Add to cart
              </button>
              <button
                onClick={() => this.props.handleChangeDetail(dataShoe)}
                className="btn btn-warning"
                data-toggle="modal"
                data-target="#exampleModal"
              >
                View Detail
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      let action = {
        type: ADD_SHOE,
        payload: shoe,
      };
      dispatch(action);
    },
    handleChangeDetail: (shoe) => {
      let action = {
        type: CHANGE_DETAIL,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(Itemshoe);
