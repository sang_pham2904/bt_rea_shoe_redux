export const ADD_SHOE = "ADD_SHOE";
export const CHANGE_QUANTITY = "CHANGE_QUANTITY";
export const DELETE = "DELETE";
export const CHANGE_DETAIL = "CHANGE_DETAIL";
export const BUY = "BUY";
