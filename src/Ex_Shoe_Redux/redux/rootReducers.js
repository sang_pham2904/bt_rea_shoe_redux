import { combineReducers } from "redux";
import shoeReducer from "./shoeReducer";

export const rootReducers = combineReducers({
  shoeReducer,
});
