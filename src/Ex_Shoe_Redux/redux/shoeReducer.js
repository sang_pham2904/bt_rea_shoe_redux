import { shoeArr } from "../data";
import {
  ADD_SHOE,
  BUY,
  CHANGE_DETAIL,
  CHANGE_QUANTITY,
  DELETE,
} from "./constant/shoeConstant";
const initialState = {
  shoeArr: shoeArr,
  cart: [],
  shoe: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index == -1) {
        action.payload.soLuong = 1;
        cloneCart.push(action.payload);
      } else {
        cloneCart[index].soLuong++;
      }

      return { ...state, cart: cloneCart };
    }
    case CHANGE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index != -1) {
        cloneCart[index].soLuong =
          cloneCart[index].soLuong + action.payload.num;
      }
      if (cloneCart[index].soLuong < 1) {
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }
    case DELETE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload;
      });
      if (index != -1) {
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }
    case CHANGE_DETAIL: {
      let cloneShoe = { ...state.shoe };
      cloneShoe = action.payload;
      return { ...state, shoe: cloneShoe };
    }
    case BUY: {
      let cloneCart = [...state.cart];
      cloneCart = [];
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
